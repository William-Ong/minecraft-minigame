/****************************************
*AUTHOR: MING HANG ONG                  *
*ID: 19287368							*
*DATE MODIFIED: 27/10/2019				*
*****************************************/
/***************************************************************************
* Note: This is a heavily modified code from sample 2 given from Blackboard*
***************************************************************************/
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <learnopengl/filesystem.h>
#include <learnopengl/shader_m.h>
#include <learnopengl/game_camera.h>

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

using namespace std;

// Box coordinate with 36 vertices.
// Every 3 coordinates will form 1 triangle.
// The last 2 columns represent texture coordinate for mapping.
float box[] = {
	// positions          // normals           // texture coords
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
	 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
	-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
	-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
	-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
	-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
	 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
};

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void process_input(GLFWwindow *window);
unsigned int loadTexture(char const * path);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
Camera camera(glm::vec3(0.0f, 1.0f, 50.0f));
float last_x = SCR_WIDTH / 2.0f;
float last_y = SCR_HEIGHT / 2.0f;
bool first_mouse = true;

// restart
int RESTART_DELAY = 0;

// lighting
glm::vec3 light_pos(0.0f, 0.2f, 48.0f);
const float light_increment = 0.2f;
float light_strength = 1.0f;

// timing
float delta_time = 0.0f;	// time between current frame and last frame
float last_frame = 0.0f;

// Toggle (Animation or states)
bool BUTTON_PRESSED = false;
int BUTTON_DELAY = 0;
bool BUTTON_CLOSE_ENOUGH = false;

// Toggle coordinate
bool SHOW_COORDINATE = false;
int SHOW_DELAY = 0;

// Pick up torch
bool PICK_UP = false;
int PICK_UP_DELAY = 0;
bool LAMP_CLOSE_ENOUGH = false;

// Toggle ambient lighting
bool TOGGLE_BRIGHT = false;
int TOGGLE_BRIGHT_DELAY = 0;

// Toggle sven picked up
bool SVEN_PICKED_UP = false;
bool SVEN_CLOSE_ENOUGH = 0;

// Toggle projection
bool TOGGLE_PROJECTION = false;
float TOGGLE_PROJECTION_DELAY = 0.0;

// Toggle game status
bool TOGGLE_END = false;

// Toggle trap
float trap_translate_y = 0.0f;

// Toggle teleport
bool TELEPORTED = false;
int teleport_rng;

// Sven and Sheep variables
glm::vec3 sven_initial_position(0.0f, 0.4f, 2.411f);
glm::vec3 sheep_initial_position( 0.0f,  0.71f, 0.0f);
glm::vec3 sheep_updated_position(0.0f, 0.0f, 0.0f);
glm::vec3 sheep_direction(0.0f, 0.0f, 1.0f);

// Sheep Movement
float sheep_move_x = 0, sheep_move_z = 0;


// Countdown until the button trigger can be pressed again.
// This prevents accidental burst repeat clicking of the key.
void update_delay()
{
	if(BUTTON_DELAY > 0) BUTTON_DELAY -= 1;
	if(SHOW_DELAY > 0) SHOW_DELAY -= 1;
    if(PICK_UP_DELAY > 0) PICK_UP_DELAY -= 1;
    if(TOGGLE_BRIGHT_DELAY > 0) TOGGLE_BRIGHT_DELAY -= 1;
    if(TOGGLE_PROJECTION_DELAY > 0) TOGGLE_PROJECTION_DELAY -= 1;
    if(RESTART_DELAY > 0) RESTART_DELAY -= 1;
}

// Toggle button pressing only if the camera is close enough.
void toggle_button_distance(glm::vec3 button_pos)
{
	if(glm::length(camera.Position - button_pos) <= 1.6f)
		BUTTON_CLOSE_ENOUGH = true;
	else
		BUTTON_CLOSE_ENOUGH = false;
}

// Toggle picking up torch only if camera is close enough
void toggle_pickup_distance(glm::vec3 torch_pos)
{
    if(glm::length(camera.Position - torch_pos) <= 1.6f)
        LAMP_CLOSE_ENOUGH = true;
    else
        LAMP_CLOSE_ENOUGH = false;
}

// Toggle picking up sven only if camera is close enough
void toggle_sven_distance(glm::vec3 sven_pos)
{
    if(glm::length(camera.Position - sven_pos) <= 1.6f)
        SVEN_CLOSE_ENOUGH = true;
    else
        SVEN_CLOSE_ENOUGH = false;
}

// Toggle sheep distance to player
void toggle_sheep_distance(glm::vec3 sheep_pos)
{
	if(glm::length(camera.Position - sheep_pos) <= 1.0f)
		TOGGLE_END = true;
	else
		TOGGLE_END = false;
}

// Generate a value between -50 to 50, the size of the map
int rng()
{
	// Generate a random position for the teleport within the size of the map
	std::random_device dev;
   	std::mt19937 rng(dev());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(-50,50); // distribution in range [1, 6]
    return (int)dist6(rng);
}

// Reset all the variables to it's initial values
void restart()
{
	// camera
	camera.Position = glm::vec3(0.0f, 1.0f, 50.0f);
	last_x = SCR_WIDTH / 2.0f;
	last_y = SCR_HEIGHT / 2.0f;
	first_mouse = true;

	// lighting
	light_pos = glm::vec3(0.0f, 0.2f, 48.0f);
	light_strength = 1.0f;

	// timing
	delta_time = 0.0f;	// time between current frame and last frame
	last_frame = 0.0f;

	// Toggle (Animation or states)
	BUTTON_PRESSED = false;
	BUTTON_DELAY = 0;
	BUTTON_CLOSE_ENOUGH = false;

	// Toggle coordinate
	SHOW_COORDINATE = false;
	SHOW_DELAY = 0;

	// Pick up torch
	PICK_UP = false;
	PICK_UP_DELAY = 0;
	LAMP_CLOSE_ENOUGH = false;

	// Toggle ambient lighting
	TOGGLE_BRIGHT = false;
	TOGGLE_BRIGHT_DELAY = 0;

	// Toggle sven picked up
	SVEN_PICKED_UP = false;
	SVEN_CLOSE_ENOUGH = 0;

	// Toggle projection
	TOGGLE_PROJECTION = false;
	TOGGLE_PROJECTION_DELAY = 0.0;

	// Toggle end game
	TOGGLE_END = false;
	TELEPORTED = false;

	// Regenerate new teleport position
	teleport_rng = rng();

	// Restart mob variables
	sven_initial_position = glm::vec3(0.0f, 0.4f, 2.411f);
	sheep_initial_position = glm::vec3( 0.0f,  0.71f, 0.0f);
	sheep_updated_position = glm::vec3(0.0f, 0.0f, 0.0f);
	sheep_move_x = 0;
	sheep_move_z = 0;

	// Reset camera view
	camera.reset();
}

// Main method that loads all the function and does all the renderig
int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "OpenGL Tutorial", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    // tell GLFW to capture our mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// configure global opengl state
	// -----------------------------
	glEnable(GL_DEPTH_TEST);

	// build and compile our shader program
	// ------------------------------------
	Shader lighting_shader("./game.vs", "./game.fs");
	Shader torch_shader("./torch.vs", "./torch.fs");

	// set up vertex data (and buffer(s)) and configure vertex attributes
	// ------------------------------------------------------------------

	unsigned int VBO_box, VAO_box;

	glGenVertexArrays(1, &VAO_box);
	glGenBuffers(1, &VBO_box);

	glBindVertexArray(VAO_box);

	glBindBuffer(GL_ARRAY_BUFFER, VBO_box);
	glBufferData(GL_ARRAY_BUFFER, sizeof(box), box, GL_STATIC_DRAW);

	//vertex coordinates
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	//normal vectors
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	//texture coordinates
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);



	// second, configure the light's VAO (VBO stays the same; the vertices are the same for the light object which is also a 3D cube)
	unsigned int VAO_light;
	glGenVertexArrays(1, &VAO_light);
	glBindVertexArray(VAO_light);

	glBindBuffer(GL_ARRAY_BUFFER, VBO_box);
	// note that we update the torch's position attribute's stride to reflect the updated buffer data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	
	// load and create a texture
	// -------------------------
	unsigned int tex_red_diffuse, tex_green_diffuse, tex_blue_diffuse, tex_grass_diffuse;
	unsigned int tex_red_specular, tex_green_specular, tex_blue_specular, little_specular;
    unsigned int tex_bark, tex_leaves, tex_watersheep_color, tex_watersheep_face, tex_sven_color, 
    tex_sven_face, tex_losing, tex_winning, tex_metal, tex_teleport, tex_sky, tex_bjark;

	tex_grass_diffuse = loadTexture(FileSystem::getPath("resources/textures/grass.jpg").c_str());
	little_specular = loadTexture(FileSystem::getPath("resources/textures/grass_specular.jpg").c_str());
	tex_red_diffuse = loadTexture(FileSystem::getPath("resources/textures/red.jpg").c_str());
	tex_red_specular = loadTexture(FileSystem::getPath("resources/textures/red_specular.jpg").c_str());
	tex_green_diffuse = loadTexture(FileSystem::getPath("resources/textures/green.jpg").c_str());
	tex_green_specular = loadTexture(FileSystem::getPath("resources/textures/green_specular.jpg").c_str());
	tex_blue_diffuse = loadTexture(FileSystem::getPath("resources/textures/blue.jpg").c_str());
	tex_blue_specular = loadTexture(FileSystem::getPath("resources/textures/blue_specular.jpg").c_str());
    tex_bark = loadTexture(FileSystem::getPath("resources/textures/bark.jpg").c_str());
    tex_leaves = loadTexture(FileSystem::getPath("resources/textures/leaves.jpg").c_str());
    tex_watersheep_color = loadTexture(FileSystem::getPath("resources/textures/grey_wool.jpg").c_str());
    tex_watersheep_face = loadTexture(FileSystem::getPath("resources/textures/watersheep_face.jpg").c_str());
    tex_sven_color = loadTexture(FileSystem::getPath("resources/textures/lead_gray.jpg").c_str());
    tex_sven_face = loadTexture(FileSystem::getPath("resources/textures/sven_face.png").c_str());
    tex_losing = loadTexture(FileSystem::getPath("resources/textures/losing.jpg").c_str());
    tex_winning = loadTexture(FileSystem::getPath("resources/textures/winning.jpg").c_str());
    tex_metal = loadTexture(FileSystem::getPath("resources/textures/metal.png").c_str());
    tex_teleport = loadTexture(FileSystem::getPath("resources/textures/teleport.jpg").c_str());
    tex_sky = loadTexture(FileSystem::getPath("resources/textures/night_sky.jpg").c_str());
    tex_bjark = loadTexture(FileSystem::getPath("resources/textures/bjark.png").c_str());

	//shader configuration -------------------------------------------------------------------------------------------
	lighting_shader.use();
	lighting_shader.setInt("material.diffuse", 0);
	lighting_shader.setInt("material.specular", 1);

	//retrieve random-generated position of teleport
	teleport_rng = rng();
	
	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// per-frame time logic
		// --------------------
		float currentFrame = glfwGetTime();
		delta_time = currentFrame - last_frame;
		last_frame = currentFrame;

		// update delay countdown
		update_delay();

		// input
		// -----
		process_input(window);

		// render
		// ------
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// pendulum animation
		//----------------------------
		static float direction = 1.0f;
        float pendulum;
        if (pendulum > 25.0f)
            direction = -1.0f;
        if (pendulum < -25.0f)
            direction = 1.0f;
        pendulum += 2.0 * direction;

		// activate shader
		lighting_shader.use();
        if(PICK_UP == true)
        {
            // Save light positions to player
            light_pos = camera.Position;
            lighting_shader.setVec3("light.position", light_pos);
        }
        else
        {
        	lighting_shader.setVec3("light.position", light_pos);
        }
        lighting_shader.setVec3("viewPos", camera.Position);

		// light properties
		if(TOGGLE_BRIGHT == true)
            //Set all texture to extremely bright
            lighting_shader.setVec3("light.ambient", 1.0f, 1.0f, 1.0f);
        else
            //Set all texture to darkish 
            lighting_shader.setVec3("light.ambient", 0.05f, 0.05f, 0.05f);

		lighting_shader.setVec3("light.diffuse", 0.8f, 0.8f, 0.8f);
		lighting_shader.setVec3("light.specular", 1.0f, 1.0f, 1.0f);
		lighting_shader.setFloat("light.strength", light_strength);

		// material properties
        lighting_shader.setFloat("material.shininess", 65.0f);
		// for now just set the same for every object. But, you can make it dynamic for various objects.


		// camera/view transformation
		glm::mat4 projection;
		if(TOGGLE_PROJECTION == true)
		{
			// orthographic projection
			projection = glm::ortho(-5.0f, 5.0f, -4.0f, 4.0f, -45.0f, 100.0f);
		}
		else
		{
			// perspective projection
			projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 300.0f);
		}

		// construct the camera view and projector
  		glm::mat4 view = camera.GetViewMatrix();
        lighting_shader.setMat4("projection", projection);
		lighting_shader.setMat4("view", view);

        // start jump
        camera.jump();

		// declare transformation matrix/world transformation
		glm::mat4 model = glm::mat4();
        lighting_shader.setMat4("model", model);

		// Draw objects
		//---------------------------------------------------------------------

		//---------------------------------------------------------------------
		// Sky
		glBindVertexArray(VAO_box);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex_sky);

		model = glm::mat4();
		model = glm::scale(model, glm::vec3(200.0f, 200.0f, 200.0f));

		lighting_shader.setMat4("model", model);

		glDrawArrays(GL_TRIANGLES, 0, 36);
		// End of Sky
		//---------------------------------------------------------------------

        //---------------------------------------------------------------------
		// Coordinate System
		if(SHOW_COORDINATE == true)
		{

			glm::vec3 coord_scales[] = {
				glm::vec3( 100.0f,  0.02f,  0.02f),	//X
				glm::vec3( 0.02f,  100.0f,  0.02f),	//Y
				glm::vec3( 0.02f,  0.02f,  100.0f),	//Z
			};

			glBindVertexArray(VAO_box);

			for(int tab = 0; tab < 3; tab++)
			{
				if(tab == 0) // X
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, tex_red_diffuse);
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, tex_red_specular);
				}
				if(tab == 1) // Y
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, tex_green_diffuse);
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, tex_green_specular);
				}
				if(tab == 2) // Z
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, tex_blue_diffuse);
					glActiveTexture(GL_TEXTURE1);
					glBindTexture(GL_TEXTURE_2D, tex_blue_specular);
				}

				model = glm::mat4();
				model = glm::scale(model, coord_scales[tab]);

				lighting_shader.setMat4("model", model);

				glDrawArrays(GL_TRIANGLES, 0, 36);
			}
		}
        // End of Coordinate System
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
		// Grass
		glBindVertexArray(VAO_box);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex_grass_diffuse);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, little_specular);

		model = glm::mat4();
		model = glm::translate(model, glm::vec3(0.0f, -0.01f, 0.0f));
		model = glm::scale(model, glm::vec3(105.0f, 0.001f, 105.0f));

		lighting_shader.setMat4("model", model);

		glDrawArrays(GL_TRIANGLES, 0, 36);
        // End of grass
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
        // Draw trees
        glm::vec3 tree_structure[] = {
        	glm::vec3(2.0f, 0.0f, 0.0f),
        	glm::vec3(2.0f, 1.25, 0.0f)
        };

        glm::vec3 tree_scale[] = {
        	glm::vec3(0.5f, 2.0f, 0.5f), // bark scale
        	glm::vec3(2.0f, 2.0f, 2.0f) // foliage scale
        };

		glBindVertexArray(VAO_box);

		for(int i = -50; i < 50; i += 5)
		{
			for(int j = -50; j < 50; j+= 5)
			{
				for(int tab = 0; tab < 2; tab++)
				{
					if(tab == 0)
					{
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, tex_bark);
		        		//Dark second layer, prevents specular properties from coming out
						glActiveTexture(GL_TEXTURE1);
						glBindTexture(GL_TEXTURE_2D, little_specular);
					}
					if(tab == 1)
					{
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, tex_leaves);
		        		//Dark second layer, prevents specular properties from coming out
						glActiveTexture(GL_TEXTURE1);
						glBindTexture(GL_TEXTURE_2D, little_specular);
					}
					model = glm::mat4();
		        	model = glm::translate(model, tree_structure[tab]);
		        	// Draw multiple trees
		        	model = glm::translate(model, glm::vec3((float)i, 0.0f, (float)j));
		        	model = glm::scale(model, tree_scale[tab]);
		        	model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));

		        	lighting_shader.setMat4("model", model);

					glDrawArrays(GL_TRIANGLES, 0, 36);
				}
			}
		}
        // End of trees
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
        // Sven 
        glm::vec3 sven_scales[] = {
            glm::vec3(0.23f, 0.2f, 0.23f),  //face
            glm::vec3(0.25f, 0.22f, 0.25f), //head
            glm::vec3(0.3f,  0.3f,  0.6f),  //torso
            glm::vec3(0.1f,  0.35f,  0.1f), //front left
            glm::vec3(0.1f,  0.35f,  0.1f), //front right
            glm::vec3(0.1f,  0.35f,  0.1f), //back left
            glm::vec3(0.1f,  0.35f,  0.1f), //back right
            glm::vec3(0.1f, 0.1f, 0.4f),    //tail
        };

        // Sets sven's position at real-time, fucking kill me
        glm::vec3 sven_positions[] = {
            glm::vec3(sven_initial_position.x, sven_initial_position.y, sven_initial_position.z), // face
            glm::vec3(sven_initial_position.x, sven_initial_position.y, sven_initial_position.z - 0.011f), // head
            glm::vec3(sven_initial_position.x, sven_initial_position.y - 0.05f, sven_initial_position.z - 0.411f), // torso
            glm::vec3(sven_initial_position.x - 0.1f, sven_initial_position.y - 0.4f, sven_initial_position.z - 0.161f), // front left
            glm::vec3(sven_initial_position.x + 0.1f, sven_initial_position.y - 0.4f, sven_initial_position.z - 0.161f), // front right
            glm::vec3(sven_initial_position.x - 0.1f, sven_initial_position.y - 0.4f, sven_initial_position.z - 0.661f), // back left
            glm::vec3(sven_initial_position.x + 0.1f, sven_initial_position.y - 0.4f, sven_initial_position.z - 0.661f), // back right
            glm::vec3(sven_initial_position.x, sven_initial_position.y + 0.13f, sven_initial_position.z - 0.821) // tail
        };

        if(!TOGGLE_END)
        {
        	glBindVertexArray(VAO_box);

	        for(int tab = 0; tab < 8; tab++)                                   
	        {                                                                       
		        glActiveTexture(GL_TEXTURE0);
	            if(tab == 0)
	            {
	                glBindTexture(GL_TEXTURE_2D, tex_sven_face);
	            }
	            else
	            {
	    	        glBindTexture(GL_TEXTURE_2D, tex_sven_color);
	            }
	            glActiveTexture(GL_TEXTURE1);
	            glBindTexture(GL_TEXTURE_2D, little_specular);

	            model = glm::mat4();
	            if(SVEN_PICKED_UP == true)
	            {
	            	// Move Sven to front of the camera
	                sven_initial_position = camera.Position + camera.Front;
	                // Fix its y-axis so it appears on the floor
	                sven_initial_position.y = 0.4f;
	                model = glm::translate(model, sven_positions[tab]);
	            }
	            else
	            {
	            	model = glm::translate(model, sven_positions[tab]);
	            }

	            if(tab == 7)
	           	{
	           		// Tail wagging animation
	           		model = glm::rotate(model, glm::radians(pendulum), glm::vec3(0.0f, 1.0f, 0.0f));
	            }
	            model = glm::scale(model, sven_scales[tab]);          
	            model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));      
	                                                                                
	            lighting_shader.setMat4("model", model);                            
	                                                                                
	            glDrawArrays(GL_TRIANGLES, 0, 36);                                  
	        }

	        toggle_sven_distance(sven_positions[0]);
        }
        // End of Sven
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
        // Traps
        glm::vec3 trap_scale[] = {
        	glm::vec3(0.4f, 0.05f, 0.4f),
        	glm::vec3(0.1f, 0.3f, 0.1f),
        	glm::vec3(0.1f, 0.3f, 0.1f),
        	glm::vec3(0.1f, 0.3f, 0.1f),
        	glm::vec3(0.1f, 0.3f, 0.1f)
        };

        glm::vec3 trap_positions[] = {
        	glm::vec3(0.0f, 0.0f, 6.0f),
        	glm::vec3(0.1f, -0.1f, 5.9f),
        	glm::vec3(-0.1f, -0.1f, 5.9f),
        	glm::vec3(0.1f, -0.1f, 6.1f),
        	glm::vec3(-0.1f, -0.1f, 6.1f)
        };

        glBindVertexArray(VAO_box);
		for(int i = -45; i < 45; i += 5)
		{
			for(int j = -45; j < 45; j+= 5)
			{
				for(int tab = 0; tab < 5; tab++)
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, tex_metal);

			       	model = glm::mat4();
			       	if(tab > 0)
			       	{
			       		// Translate trap's y-axis to make it go up and down
			       		model = glm::translate(model, glm::vec3(trap_positions[tab].x,
			       												trap_positions[tab].y + (0.25f * sin(trap_translate_y * acos(-1) / 180.0f)),
			       												trap_positions[tab].z));
			       	}
			       	else
		        	{
		        		model = glm::translate(model, trap_positions[tab]);
		        	}
			       	model = glm::translate(model, glm::vec3((float)i, 0.0f, (float)j));
			       	model = glm::scale(model, trap_scale[tab]);

			       	lighting_shader.setMat4("model", model);

					glDrawArrays(GL_TRIANGLES, 0, 36);
				}
			}
		}
		if(SVEN_PICKED_UP)
		{
	        // Controls the speed of propelled traps
	       	trap_translate_y += 6.0f;
	       	// Makes sure the trap doesn't translate forever
		    if(abs(trap_translate_y - 360.0f) <= 0.1) trap_translate_y = 0.0f;
		}
       	// End of traps
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
		// Water sheep (4 tall boxes for legs & 1 thiccc box as sheep torso)
		glm::vec3 sheep_scales[] = {
			glm::vec3( 0.28f,  0.28f,  0.3f),	//face
			glm::vec3( 0.3f,  0.3f,  0.3f),	    //head
			glm::vec3( 0.6f,  0.5f,  1.0f),	    //torso
			glm::vec3( 0.15f,  -0.4f,  0.15f),   //front left leg
			glm::vec3( 0.15f,  -0.4f,  0.15f),	//front right leg
			glm::vec3( 0.15f,  -0.4f,  0.15f),   //back left leg
			glm::vec3( 0.15f,  -0.4f,  0.15f),	//back right leg
		};

		glm::vec3 sheep_positions[] = {
			glm::vec3(sheep_initial_position.x,  sheep_initial_position.y,  sheep_initial_position.z),    //face
			glm::vec3(sheep_initial_position.x,  sheep_initial_position.y - 0.01f,  sheep_initial_position.z - 0.01f),     //head
			glm::vec3(sheep_initial_position.x,  sheep_initial_position.y - 0.36f,  sheep_initial_position.z - 0.51f),    //torso
			glm::vec3(sheep_initial_position.x - 0.21f, sheep_initial_position.y - 0.31f,  sheep_initial_position.z - 0.16f),	//front left leg
			glm::vec3(sheep_initial_position.x + 0.21f, sheep_initial_position.y - 0.31f,  sheep_initial_position.z - 0.16f),	//front right leg
			glm::vec3(sheep_initial_position.x - 0.21f, sheep_initial_position.y - 0.31f, sheep_initial_position.z - 0.86f),	//back left leg
			glm::vec3(sheep_initial_position.x + 0.21f, sheep_initial_position.y - 0.31f, sheep_initial_position.z - 0.86f),	//back right leg
		};

		// Angle-finding algorithm
		glm::vec3 temp;
		float angle = 0;
		if(camera.Position.x < sheep_updated_position.x)
		{
			temp = sheep_updated_position - camera.Position;
			// Reset so it doesn't spin forever
			angle = 180.0f;
		}
		else
		{
			// Swapped coordinate arithmetic relative to sheep's postion
			temp = camera.Position - sheep_updated_position;
		}
		// Remove sheep vibration
		temp.y = 0;
		angle += (180.0f * glm::angle(sheep_direction, glm::normalize(temp))) / acos(-1);

		if(SVEN_PICKED_UP)
		{
			glBindVertexArray(VAO_box);

			for(int tab = 0; tab < 7; tab++)
			{
				glActiveTexture(GL_TEXTURE0);
	            if(tab == 0)//Draw the face to seperate the rest
	            {
		        	glActiveTexture(GL_TEXTURE0);
	    		    glBindTexture(GL_TEXTURE_2D, tex_watersheep_face);
	            }
	            else
	            {
		        	glActiveTexture(GL_TEXTURE0);
	    		    glBindTexture(GL_TEXTURE_2D, tex_watersheep_color);
	            }
				model = glm::mat4();
				// Retrives sheep's position at real-time
			    sheep_updated_position = glm::vec3(sheep_positions[0].x + sheep_move_x,
			    								   sheep_positions[0].y,
			    								   sheep_positions[0].z + sheep_move_z);
			    // If you rotate before you translate (column-vector order), the whole composite moves
			    // Translate to move the sheep
			    model = glm::translate(model, glm::vec3(sheep_move_x, 0.0f, sheep_move_z));
			    // Rotate before moving
			   	model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
			   	// Translate model components
			    model = glm::translate(model, sheep_positions[tab]);
			   
			    if(tab == 3 || tab == 6)
	            {
	            	// Moving legs
	            	model = glm::rotate(model, glm::radians(pendulum), glm::vec3(1.0f, 0.0f, 0.0f));
	            }
	            if(tab == 4 || tab == 5)
	            {
	            	// Moving legs
	            	model = glm::rotate(model, glm::radians(-pendulum), glm::vec3(1.0f, 0.0f, 0.0f));
	            }
				model = glm::scale(model, sheep_scales[tab]);
				model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));

				lighting_shader.setMat4("model", model);

				glDrawArrays(GL_TRIANGLES, 0, 36);
			}
			toggle_sheep_distance(sheep_updated_position);
		}
        // End of sheep
        //---------------------------------------------------------------------

		// Activate animation when sven is picked up
		if(SVEN_PICKED_UP)
		{            
			float sheep_speed = 2.5f * delta_time * 0.6f;
            // Sheep movement
            // X-wise movements
            if(camera.Position.x - sheep_updated_position.x > 0.1)
                sheep_move_x += sheep_speed;
            else if(camera.Position.x - sheep_updated_position.x < -0.1)
                sheep_move_x -= sheep_speed;
            // Z-wise movements
            if(camera.Position.z - sheep_updated_position.z > 0.1)
                sheep_move_z += sheep_speed;
            else if(camera.Position.z - sheep_updated_position.z < -0.1)
                sheep_move_z -= sheep_speed;      
		}
		// End of animation
        //---------------------------------------------------------------------

		//---------------------------------------------------------------------
		// Sign post
		glm::vec3 post_postion[] = {
			glm::vec3(0.5f, 0.0f, 45.0f),
			glm::vec3(0.5f, 0.45f, 45.02f),
		    glm::vec3(0.5f, 0.465f, 45.025f)
		};

		glm::vec3 post_scale[] = {
			glm::vec3(0.05f, 0.5f, 0.05f),
			glm::vec3(0.5f, 0.35f, 0.05f),
			glm::vec3(0.45f, 0.3f, 0.045f)
		};

		for(int tab = 0; tab < 3; tab++)
		{
			glBindVertexArray(VAO_box);

			if(tab == 2)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, tex_bjark);
			}
			else
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, tex_bark);
			}
			glActiveTexture(GL_TEXTURE1);
	        glBindTexture(GL_TEXTURE_2D, little_specular);

			model = glm::mat4();
			model = glm::translate(model, post_postion[tab]);
			model = glm::rotate(model, glm::radians(-25.0f), glm::vec3(0.0f, 1.0f, 0.0f));
			model = glm::scale(model, post_scale[tab]);
			model = glm::translate(model, glm::vec3(0.0f, 0.5f, 0.0f));

			lighting_shader.setMat4("model", model);

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
		//End of sign post
		//---------------------------------------------------------------------

		//---------------------------------------------------------------------
		// End-game Screen
		glm::vec3 cam_front = camera.Position + camera.Front;
		glm::vec3 sign_positions[] = {
			cam_front,
			glm::vec3(cam_front.x, cam_front.y, cam_front.z - 0.1f)
		};

		glm::vec3 sign_scales[] = {
			glm::vec3(0.8f, 0.8f, 0.01f),
			glm::vec3(1.0f, 0.9f, 0.1f)
		};

		if(TOGGLE_END)
		{
			glBindVertexArray(VAO_box);

			for(int tab = 0; tab < 2; tab++)
			{

				if(tab == 0)
				{
					if(TELEPORTED)
					{
						// Attach winning image
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, tex_winning);
					}
					else
					{
						// Attach losing image
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, tex_losing);
					}
				}
				else
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, tex_bark);
				}

				model = glm::mat4();
				model = glm::translate(model, sign_positions[tab]);
				model = glm::scale(model, sign_scales[tab]);

				lighting_shader.setMat4("model", model);

				glDrawArrays(GL_TRIANGLES, 0, 36);
			}
			// Resets the camera so the player can't play until they've restarted the game
			camera.reset();
			// Keep it bright at all times for visibility
			TOGGLE_BRIGHT = true;
		}
		// End of screen
		//---------------------------------------------------------------------

		//---------------------------------------------------------------------
		// Escape teleport
		if(SVEN_PICKED_UP)
		{
			glBindVertexArray(VAO_box);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, tex_teleport);

			model = glm::mat4();
			model = glm::translate(model, glm::vec3((float)teleport_rng, 0.0f, (float)teleport_rng));
			model = glm::scale(model, glm::vec3(2.0f, 10000.0f, 2.0f));

			lighting_shader.setMat4("model", model);

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}	
		// End of teleport
		//---------------------------------------------------------------------

        //---------------------------------------------------------------------
        // Torch handle
		glBindVertexArray(VAO_box);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex_bark);

		model = glm::mat4();
        // Position itself to look like a torch
        model = glm::translate(model, glm::vec3(light_pos.x, light_pos.y - 0.08f, light_pos.z));
        model = glm::scale(model, glm::vec3(0.04f, 0.2f, 0.04f));

		lighting_shader.setMat4("model", model);

		glDrawArrays(GL_TRIANGLES, 0, 36);
        // End of torch handle
        //---------------------------------------------------------------------

        //---------------------------------------------------------------------
		// Draw the light source
		torch_shader.use();
		torch_shader.setMat4("projection", projection);
		torch_shader.setMat4("view", view);
		model = glm::mat4();

        if(PICK_UP)
        {
            // Save light positions to player
            model = glm::translate(model, light_pos);
        }
        else
        {
            model = glm::translate(model, light_pos);
            light_pos.y = 0.2f;
        }

    	model = glm::scale(model, glm::vec3(0.05f)); // a smaller cube
	    torch_shader.setMat4("model", model);

	    torch_shader.setVec3("inputColor", glm::vec3(0.6f, 0.2f, 0.0f));
    	torch_shader.setFloat("intensity", light_strength);

        // toggle the pick up distance
        toggle_pickup_distance(light_pos);


    	glBindVertexArray(VAO_light);
	    glDrawArrays(GL_TRIANGLES, 0, 36);
        // End of light source
        //---------------------------------------------------------------------

        // Check win condition
        if(glm::length(camera.Position - glm::vec3((float)teleport_rng, 0.0f, (float)teleport_rng)) < 1.5f && SVEN_PICKED_UP)
        {
        	TELEPORTED = true;
        	TOGGLE_END = true;
        }

	    // Game monitoring
        std::cout << "\033[2J" << std::endl;
        std::cout << "==========GAME MONITORING==========" << std::endl;
        std::cout << "Angle of Camera and sheep: " << angle << std::endl;
        std::cout << "Sheep position to Player: " << glm::length(camera.Position - sheep_updated_position) << std::endl;
        std::cout << "Light strength: " << light_strength  << std::endl;
        if(SVEN_PICKED_UP)
        {
        	std::cout << "Player position to teleport: " << glm::length(camera.Position - glm::vec3((float)teleport_rng, 0.0f, (float)teleport_rng)) << std::endl;
        }
        std::cout << "Player position to Sven: " << glm::length(camera.Position - sven_initial_position) << std::endl;
        if(TOGGLE_END)
        {
        	std::cout << "Player life: A S C E N D E D" << std::endl;
        }
        else
        {
        	std::cout << "Player life: Still alive" << std::endl;
        }
		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	glDeleteVertexArrays(1, &VAO_box);
	glDeleteBuffers(1, &VBO_box);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();
	return 0;
}



// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void process_input(GLFWwindow *window)
	{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    	glfwSetWindowShouldClose(window, true);

    if(!TOGGLE_END)
    {
	    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
	    {
	        if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	        {
	            camera.ProcessKeyboard(FORWARD, delta_time);
	        }
	        else
	        {
	            camera.ProcessKeyboard(FORWARD, delta_time * 2);
	        }
	    }
	    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
	    {
	        if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	        {
	            camera.ProcessKeyboard(BACKWARD, delta_time);
	        }
	        else
	        {
	            camera.ProcessKeyboard(BACKWARD, delta_time * 2);
	        }
	    }
	    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	    {
	        if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	        {
	            camera.ProcessKeyboard(LEFT, delta_time);
	        }
	        else
	        {
	            camera.ProcessKeyboard(LEFT, delta_time * 2);
	        }
	    }
	    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
	    {
	        if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_RELEASE)
	        {
	            camera.ProcessKeyboard(RIGHT, delta_time);
	        }
	        else
	        {
	            camera.ProcessKeyboard(RIGHT, delta_time * 2);
	        }
	    }
	    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	    {
	        camera.ProcessKeyboard(JUMP, delta_time);
	    }
	    
	    //toggle pick up
	    if(glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS && PICK_UP_DELAY == 0 && LAMP_CLOSE_ENOUGH == true)
	    {
	        PICK_UP_DELAY = 20;
	        PICK_UP = !PICK_UP;
	    }

	    //toggle sven pick up
	    if(glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS && PICK_UP_DELAY == 0 && SVEN_CLOSE_ENOUGH == true)
	    {
	        PICK_UP_DELAY = 20;
	        SVEN_PICKED_UP = !SVEN_PICKED_UP;
	    }
	}
	else
	{
		SVEN_PICKED_UP = false;
	}

	//toggle coordinate visibility
	if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && SHOW_DELAY == 0)
	{
		SHOW_DELAY = 20;
		SHOW_COORDINATE = !SHOW_COORDINATE;
	}
    //toggle ambient lighting
    if(glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS && TOGGLE_BRIGHT_DELAY == 0)
    {
        TOGGLE_BRIGHT_DELAY = 20;
        TOGGLE_BRIGHT = !TOGGLE_BRIGHT;
    }
    //toggle project (parallel/perspective)
    if(glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS && TOGGLE_PROJECTION_DELAY == 0)
    {
    	TOGGLE_PROJECTION_DELAY = 20;
    	TOGGLE_PROJECTION = !TOGGLE_PROJECTION;

    }

    //toggle light increase
    if(glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
    {
    	light_strength += light_increment;
    }

    //toggle light darken
    if(glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
    {
    	if(light_strength - light_increment < 0)
    	{
    		light_strength = 0.0f;
    	}
    	else
    	{
    		light_strength -= light_increment;
    	}
    }

    //toggle restart
    if(glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS && RESTART_DELAY == 0)
    {
    	RESTART_DELAY = 20;
    	restart();
    }
}


// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

unsigned int loadTexture(char const * path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	return textureID;
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (first_mouse)
    {
        last_x = xpos;
        last_y = ypos;
        first_mouse = false;
    }

    float xoffset = xpos - last_x;
    float yoffset = last_y - ypos; // reversed since y-coordinates go from bottom to top

    last_x = xpos;
    last_y = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}
