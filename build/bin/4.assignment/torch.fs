#version 330 core
out vec4 FragColor;

uniform float intensity;

in vec3 color;

void main()
{
    FragColor = vec4(color, 1.0) * intensity;
}
